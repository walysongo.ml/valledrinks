import React, { useState } from 'react';
import api from '../../services/api';


export default function Login({ history }) {
  const [
    nome,
    setNome,
    setCnpjCpf,
    cnpjcpf,
    email,
    setEmail,
    senha,
    setSenha
  ] = useState('');

  async function handleSubmit(event) {
    event.preventDefault();

    const response = await api.post('/sessions', { nome, email, senha, cnpjcpf });

    const { _id } = response.data;

    localStorage.setItem('user', _id);

    history.push('/dashboard');
  }

  return (
    <>
      <p>
        Venda <strong>DRINKS</strong>  e encontre uma <strong>NOVA</strong> forma para ganhar o seu dinheiro
      </p>

      <form onSubmit={handleSubmit}>
        <label htmlFor="nome">NOME *</label>
        <input
          id="nome"
          type="text"
          placeholder="O nome dá sua querida empresa"
          onChange={event => setNome(event.target.value)}
        />

        <label htmlFor="cnpjoucpf">CNPJ / CPF</label>
        <input
          id="cnpjoucpf"
          type="number"
          placeholder="Seu CNPJ ou CPF"
          value={cnpjcpf}
          onChange={event => setCnpjCpf(event.target.value)}
        />

        <label htmlFor="email">E-MAIL *</label>
        <input
          id="email"
          type="email"
          placeholder="Seu melhor e-mail"
          value={email}
          onChange={event => setEmail(event.target.value)}
        />

        <label htmlFor="senha">SENHA *</label>
        <input
          id="password"
          type="password"
          placeholder="Sua melhor senha"
          value={senha}
          onChange={event => setSenha(event.target.value)}
        />

        <button className="btn" type="submit">Entrar</button>
      </form>
    </>
  )
}