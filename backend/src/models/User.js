const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  nomeCompany: {
		type: String,
		required: true,
		select: true,
		uppercase: true,
	},
	cnpj: {
		type: String,
		required: false,
		select: false,
	},
	cpf: {
		type: String,
		required: false,
		select: false,
	},
	foto: {
		type: String,
		required: false,
	},
	email: {
		type: String,
		unique: true,
		required: true,
		lowercase: true,
	},
	password: {
		type: String,
		required: true,
		select: false,

	},
	telefone: {
		type: String,
		required: true,
		select: true,
	},
	createdAt: {
		type: Date,
		default: Date.now,
	}
});

module.exports = mongoose.model('User', UserSchema);