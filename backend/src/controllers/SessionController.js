const User = require('../models/User');

module.exports = {
  async store(req, res) {
    const { nomeCompany, email, cc, password, telefone  } = req.body;

    let user = await User.findOne({ email });

    if (!user) {
      user = await User.create({ nomeCompany, email, cc, password, telefone  });
    }

    return res.json(user);
  }
};
